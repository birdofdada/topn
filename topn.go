package main

import (
	"bufio"
	"container/heap"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
)

func main() {
	file := flag.String("f", "testdata/file.1", "file with numbers")
	n := flag.Int("n", 10, "output the n largest numbers")
	flag.Parse()

	fp, err := os.Open(*file)
	if err != nil {
		log.Fatal(err)
	}

	for _, num := range topN(fp, *n) {
		fmt.Println(num)
	}
}

func topN(data io.Reader, N int) []int {
	// just in order to avoid redundant slice grow
	// since we know the heap size, just allocating the enough room for it at the start
	h := make(minHeap, 0, N)
	heap.Init(&h)

	scanner := bufio.NewScanner(data)
	for scanner.Scan() {
		num, err := strconv.Atoi(scanner.Text())
		if err != nil {
			// error handeling should be better
			// but I suppose it isn't  the point of the task
			continue
		}

		// fill the heap with first `N` values
		// when heap is "full", check if number greater heap's top value, and if so,
		// pop the top value and push the new one.
		if h.Len() < N {
			heap.Push(&h, num)
		} else if num > h[0] {
			heap.Pop(&h)
			heap.Push(&h, num)
		}
	}

	// in order to return highest numbers first
	// just pop out numbers from heap into the end of slice
	rlen := N
	// in case N > file lines
	if h.Len() < N {
		rlen = h.Len()
	}
	ret := make([]int, rlen)
	for j := rlen - 1; j >= 0; j-- {
		ret[j] = heap.Pop(&h).(int)
	}

	return ret
}

// An minHeap is a min-heap of ints.
type minHeap []int

func (h minHeap) Len() int           { return len(h) }
func (h minHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h minHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *minHeap) Push(x interface{}) {
	*h = append(*h, x.(int))
}

func (h *minHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
