package main

import (
	"io"
	"reflect"
	"strings"
	"testing"
)

func TestTopN(t *testing.T) {
	type args struct {
		file io.Reader
		num  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "test5",
			args: args{
				file: strings.NewReader(`
357
11
929
666
13
42
4213666
95812
64383
23374505
283735
391036473
9178392273
12
3145
654418
132435
658396
12377643
83465
2348
3453
				`),
				num: 5,
			},
			want: []int{9178392273, 391036473, 23374505, 12377643, 4213666},
		},
		{
			name: "test2",
			args: args{
				file: strings.NewReader(`
357
11
83465
2348
3453
				`),
				num: 2,
			},
			want: []int{83465, 3453},
		},
		{
			name: "test6outof5",
			args: args{
				file: strings.NewReader(`
357
11
83465
2348
3453
				`),
				num: 6,
			},
			want: []int{83465, 3453, 2348, 357, 11},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := topN(tt.args.file, tt.args.num); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("topN() = %v, want %v", got, tt.want)
			}
		})
	}
}
